package jetbrains.mps.grammarLanguage.grammar;

public class Transformation {
    public static final TransformationType LEFT = TransformationType.LEFT;
    public static final TransformationType RIGHT = TransformationType.RIGHT;

    public TransformationType getType() {
        return type;
    }

    public NonTerminal getForNonTerminal() {
        return forNonTerminal;
    }

    public GrammarRule getFromRule() {
        return fromRule;
    }

    public GrammarRule getToRule() {
        return toRule;
    }

    private enum TransformationType {
        LEFT, RIGHT;

        @Override
        public String toString() {
            switch (this) {
                case LEFT:
                    return "left";
                case RIGHT:
                    return "right";
            }
            throw new UnsupportedOperationException("Unknown transformation type:" + this);
        }
    }

    private TransformationType type;
    private NonTerminal forNonTerminal;
    private GrammarRule fromRule;
    private GrammarRule toRule;

    public Transformation(TransformationType type, NonTerminal nonTerminal, GrammarRule from, GrammarRule to) {
        this.type = type;
        this.forNonTerminal = nonTerminal;
        this.fromRule = from;
        this.toRule = to;
    }
}