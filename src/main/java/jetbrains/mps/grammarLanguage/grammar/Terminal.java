package jetbrains.mps.grammarLanguage.grammar;

public class Terminal implements Symbol {

    private String representation;

    public Terminal(String representation) {
        this.representation = representation;
    }

    @Override
    public Boolean isTerminal() {
        return true;
    }

    @Override
    public String getRepresentation() {
        return representation;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Terminal))
            return false;
        return representation.equals(((Terminal) obj).representation);
    }
}