package jetbrains.mps.grammarLanguage.grammar;

import java.util.HashSet;
import java.util.List;

public class GrammarRule {
    private int incompleteRuleNumber = 1;
    private String name;
    private NonTerminal from;
    private List<Symbol> to;

    public GrammarRule(String name, NonTerminal from, List<Symbol> to) {
        this.name = name;
        this.from = from;
        this.to = to;
    }

    public String getName() {
        return name;
    }

    public NonTerminal getFrom() {
        return from;
    }

    public List<Symbol> getTo() {
        return to;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof GrammarRule))
            return false;
        return from.equals(((GrammarRule) obj).from) && to.equals(((GrammarRule) obj).to);
    }

    @Override
    public int hashCode() {
        final int prime = 37;
        return from.hashCode() + prime * to.hashCode();
    }

    public int length() {
        return to.size();
    }

    public boolean isUseless() {
        return to.size() == 1 && from.equals(to.get(0));
    }

    public boolean leftTransformTo(GrammarRule rule) {
        return from.equals(rule.from) &&
                to.size() + 1 == rule.to.size() &&
                rule.to.subList(1, rule.to.size()).equals(to);
    }

    public boolean rightTransformTo(GrammarRule rule) {
        return from.equals(rule.from) &&
                to.size() + 1 == rule.to.size() &&
                rule.to.subList(0, rule.to.size() - 1).equals(to);
    }

    public HashSet<GrammarRule> getAdditionalRules() {
        var result = new HashSet<GrammarRule>();
        for (int i = 1; i + 1 < to.size(); i++) {
            result.add(new GrammarRule(name + incompleteRuleNumber++, from, to.subList(0, i)));
            result.add(new GrammarRule(name + incompleteRuleNumber++, from, to.subList(i, to.size())));
        }
        for (Symbol symbol: to) {
            if (symbol.isTerminal()) {
                result.add(new GrammarRule(name + incompleteRuleNumber++, from, List.of(symbol)));
            }
        }
        return result;
    }
}