package jetbrains.mps.grammarLanguage.grammar;

public class NonTerminal implements Symbol {

    private String representation;

    public NonTerminal(String representation) {
        this.representation = representation;
    }

    @Override
    public Boolean isTerminal() {
        return false;
    }

    @Override
    public String getRepresentation() {
        return representation;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof NonTerminal))
            return false;
        return representation.equals(((NonTerminal) obj).representation);
    }
}