package jetbrains.mps.grammarLanguage.grammar;

import java.util.*;
import java.util.stream.Collectors;

public class Grammar {
    private List<GrammarRule> rules;
    private List<GrammarRule> additionalRules;
    private List<Transformation> transformations;

    public Grammar(List<GrammarRule> rules) {
        this.rules = rules;
    }

    public List<GrammarRule> getAdditionalRules() {
        return additionalRules;
    }

    public List<Transformation> getTransformations() {
        return transformations;
    }

    private void generateAdditionalRules() {
        var addRules = new HashSet<GrammarRule>();
        for (var rule: rules) {
            addRules.addAll(rule.getAdditionalRules());
        }
        addRules.removeAll(rules);
        additionalRules = new ArrayList<>(addRules);
    }

    private void generateTransformations() {
        transformations = new ArrayList<>(transformations);
        var allRules = new ArrayList<>(rules);
        allRules.addAll(additionalRules);
        var rulesByNonTerminal = allRules.stream().collect(
                Collectors.groupingBy(GrammarRule::getFrom, Collectors.groupingBy(GrammarRule::length)));
        for (var nonTerminal: rulesByNonTerminal.keySet()) {
            var rulesByLength = rulesByNonTerminal.get(nonTerminal);
            var setOfLengths = new TreeSet<>(rulesByLength.keySet());
            for (var length: setOfLengths) {
                var fromSet = rulesByLength.get(length);
                var toSet = rulesByLength.get(length + 1);
                if (fromSet == null || toSet == null)
                    continue;
                for (var fromRule: fromSet) {
                    for (var toRule: toSet) {
                        if (fromRule.leftTransformTo(toRule)) {
                            transformations.add(
                                    new Transformation(Transformation.LEFT, nonTerminal, fromRule, toRule));
                        }
                        if (fromRule.rightTransformTo(toRule)) {
                            transformations.add(
                                    new Transformation(Transformation.RIGHT, nonTerminal, fromRule, toRule));
                        }
                    }
                }
            }
        }
    }

    public void adaptGrammar() {
        generateAdditionalRules();
        generateTransformations();
        additionalRules.removeIf(GrammarRule::isUseless);
    }
}