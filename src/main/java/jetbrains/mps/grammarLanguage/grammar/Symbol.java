package jetbrains.mps.grammarLanguage.grammar;

public interface Symbol {
    Boolean isTerminal();
    String getRepresentation();
}